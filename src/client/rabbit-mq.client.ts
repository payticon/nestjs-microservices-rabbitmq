import { Injectable } from '@nestjs/common';
import { randomStringGenerator } from '@nestjs/common/utils/random-string-generator.util';
import { ClientProxy, ReadPacket, WritePacket } from '@nestjs/microservices';
import { Options } from 'amqplib/properties';
import { AmqpConnectionWrapper } from '../utils/amqp-connection.wrapper';
import { take } from 'rxjs/operators';

interface IRabbitMqClientOptions {
  disconnectAfterSend?: boolean;
}

@Injectable()
export class RabbitMqClient extends ClientProxy {
  public static createFromWrapper(wrapper: AmqpConnectionWrapper, options?: IRabbitMqClientOptions): RabbitMqClient {
    return new RabbitMqClient(wrapper, options);
  }

  public static createFromUrl(url: string, options?: IRabbitMqClientOptions): RabbitMqClient {
    const wrapper = AmqpConnectionWrapper.createFromUrl(url);
    return new RabbitMqClient(wrapper, options);
  }

  private constructor(public wrapper: AmqpConnectionWrapper, public options: IRabbitMqClientOptions = {}) {
    super();
    this.initializeSerializer({});
    this.initializeDeserializer({});
  }

  public close(): Promise<void> {
    return this.wrapper.disconnect();
  }

  public connect(): Promise<void> {
    if (this.wrapper.connected) {
      return Promise.resolve();
    }
    return new Promise((resolve, reject) => {
      this.wrapper
        .connect()
        .pipe(take(1))
        .subscribe(resolve, reject);
    });
  }

  protected async dispatchEvent<T = any>(packet: ReadPacket<any>): Promise<any> {
    const { pattern, data } = this.serializer.serialize(packet);
    const payload = Buffer.from(JSON.stringify(data));

    this.sendMessage(pattern, payload, {});

    if (this.options.disconnectAfterSend) {
      this.close();
    }
  }

  protected publish(packet: ReadPacket<any>, callback: (packet: WritePacket) => void) {
    const { pattern, data } = this.serializer.serialize(packet);

    if (pattern.exchange) {
      throw new Error('Cannot publish rpc event to exchange, use queue');
    }

    const payload = Buffer.from(JSON.stringify(data));
    const correlationId = randomStringGenerator();
    const replyTo = `${pattern.queue}_replay`;
    let consumerTag: string;

    this.wrapper.channel
      .consume(replyTo, message => {
        if (!message) {
          return;
        }
        const content = JSON.parse(message.content.toString());
        const response = this.deserializer.deserialize(content);

        if (message.properties.correlationId === correlationId) {
          callback(response);
          this.wrapper.channel.ack(message);
        }
      })
      .then(({ consumerTag: tag }) => (consumerTag = tag));

    this.sendMessage(pattern, payload, {
      replyTo,
      correlationId,
    });

    return async () => {
      await this.wrapper.channel.cancel(consumerTag);
      if (this.options.disconnectAfterSend) {
        await this.close();
      }
    };
  }

  private sendMessage(pattern: any, payload: Buffer, options: Options.Publish) {
    if (pattern.exchange) {
      return this.wrapper.channel.publish(pattern.exchange, pattern.routingKey, payload, options);
    } else if (pattern.queue) {
      return this.wrapper.channel.sendToQueue(pattern.queue, payload, options);
    } else {
      throw new Error('Cannot use pattern, exchange or queue is require');
    }
  }
}
