import { CustomTransportStrategy, Server } from '@nestjs/microservices';
import { Message } from 'amqplib';
import { Observable } from 'rxjs';
import { AmqpConnectionWrapper } from '../utils/amqp-connection.wrapper';
import { switchMap } from 'rxjs/operators';

export class RabbitMqServer extends Server implements CustomTransportStrategy {
  public static createFromWrapper(wrapper: AmqpConnectionWrapper): RabbitMqServer {
    return new RabbitMqServer(wrapper);
  }

  public static createFromUrl(url: string): RabbitMqServer {
    const wrapper = AmqpConnectionWrapper.createFromUrl(url);
    return new RabbitMqServer(wrapper);
  }

  private patternsMap = new Map<string, string>();

  constructor(public wrapper: AmqpConnectionWrapper) {
    super();
  }

  public close(): Promise<void> {
    return this.wrapper.disconnect();
  }

  public listen(callback: () => void): void {
    this.wrapper
      .connect()
      .pipe(switchMap(() => this.setupHandlers()))
      .subscribe(callback);
  }

  private async consume(queue: string): Promise<void> {
    await this.wrapper.channel.consume(queue, async message => {
      if (!message) {
        return;
      }
      const { content } = message;
      const messageObj = JSON.parse(content.toString());
      const pattern = this.patternsMap.get(queue);

      if (!pattern) {
        throw new Error(`Pattern for ${queue} not found`);
      }
      const handler = this.getHandlerByPattern(pattern);

      if (!handler) {
        throw new Error(`handler ${pattern} for pattern not found`);
      }

      const response$ = this.transformToObservable(await handler(messageObj)) as Observable<any>;
      this.wrapper.channel.ack(message);
      if (response$) {
        this.send(response$, data => {
          this.sendMessage(message, data);
        });
      }
    });
  }

  private sendMessage(message: Message, data: any) {
    const buffer = Buffer.from(JSON.stringify(data));
    this.wrapper.channel.sendToQueue(message.properties.replyTo, buffer, {
      correlationId: message.properties.correlationId,
    });
  }

  private async setupHandlers(): Promise<void> {
    for (const p of this.getHandlers().keys()) {
      const pattern = JSON.parse(p);
      const queue: string = pattern.queue;
      const handler = this.getHandlerByPattern(pattern);
      await this.wrapper.channel.assertQueue(queue);
      this.patternsMap.set(queue, p);

      if (!handler) {
        throw new Error(`handler ${p} for pattern not found`);
      }

      if (pattern.source) {
        await this.wrapper.channel.assertExchange(pattern.source, 'topic');
        await this.wrapper.channel.bindQueue(pattern.queue, pattern.source, pattern.routingKey);
      }

      if (!handler.isEventHandler) {
        await this.wrapper.channel.assertQueue(`${pattern.queue}_replay`);
      }
      await this.consume(pattern.queue);
    }
  }
}
