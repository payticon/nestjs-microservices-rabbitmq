import { EventEmitter } from 'events';
import { Logger } from '@nestjs/common';
import { Channel, connect, Connection } from 'amqplib';
import { Options } from 'amqplib/properties';
import { merge, Observable, of, Subject } from 'rxjs';
import { delay, filter, retryWhen, switchMap, takeUntil, tap } from 'rxjs/operators';

export interface IAmqpConnectionWrapperOptions {
  doReconnect?: boolean;
  reconnectDelay?: number;
  prefetch?: number;
  showLogs?: boolean;
}

export class AmqpConnectionWrapper extends EventEmitter {
  public static createFromOptions(
    connectOptions: Options.Connect,
    wrapperOptions: IAmqpConnectionWrapperOptions = {},
    socketOptions?: any,
  ): AmqpConnectionWrapper {
    const wrapper = new AmqpConnectionWrapper();
    wrapper.connectOptions = connectOptions;
    wrapper.socketOptions = socketOptions;
    wrapper.connected = false;
    wrapper.connecting = false;

    Object.assign(wrapper, wrapperOptions);

    return wrapper;
  }

  public static createFromUrl(
    url: string,
    wrapperOptions: IAmqpConnectionWrapperOptions = {},
    socketOptions?: any,
  ): AmqpConnectionWrapper {
    const wrapper = new AmqpConnectionWrapper();
    wrapper.url = url;
    wrapper.socketOptions = socketOptions;
    wrapper.connected = false;
    wrapper.connecting = false;

    Object.assign(wrapper, wrapperOptions);

    return wrapper;
  }

  public url?: string;
  public connectOptions?: Options.Connect;
  public socketOptions?: any;
  public connected = false;
  public connecting = false;
  public doReconnect = true;
  public showLogs = true;
  public reconnectDelay = 1000;
  public prefetch?: number;

  public get connection(): Connection {
    if (!this.connected) {
      throw new Error('Cannot get connection, you are not connected');
    }
    if (!this.conn) {
      throw new Error('Logic error: Nullable amqp connection');
    }

    return this.conn;
  }

  public get channel(): Channel {
    if (!this.connected) {
      throw new Error('Cannot get channel, you are not connected');
    }
    if (!this.chan) {
      throw new Error('Logic error: Nullable amqp channel');
    }

    return this.chan;
  }

  protected conn?: Connection;
  protected chan?: Channel;
  protected reconnect$ = new Subject();
  private readonly logger = new Logger(AmqpConnectionWrapper.name);

  public connect(): Observable<void> {
    if (this.connected) {
      throw new Error('Cannot connect to amqp server, already connected');
    }
    if (this.connecting) {
      throw new Error('Cannot connect to amqp server, already connecting');
    }

    const doReconnect$ = of(this.doReconnect).pipe(filter(r => !r));
    const connectOptions = this.url || this.connectOptions;
    this.connecting = true;

    if (!connectOptions) {
      throw new Error('Logic error: connection options is required');
    }

    return merge(this.reconnect$, of(true)).pipe(
      switchMap(() => connect(connectOptions, this.socketOptions)),
      retryWhen(errors =>
        errors.pipe(
          tap(e => this.logger.error('connection error:' + e.message)),
          takeUntil(doReconnect$),
          tap(() => this.log('log', 'reconnecting...')),
          delay(this.reconnectDelay),
        ),
      ),
      tap(() => this.log('log', 'amqp connected')),
      switchMap(conn => this.handleConnection(conn)),
    );
  }

  public async disconnect() {
    await this.connection.close();
  }

  protected async handleConnection(connection: Connection) {
    this.conn = connection;
    this.connected = true;
    this.connecting = false;
    this.subscribeConnectionEvents();
    await this.setupChannel();
  }

  protected subscribeConnectionEvents() {
    this.connection.removeAllListeners();
    this.connection.on('close', () => this.handleOnClose());
  }

  protected async setupChannel() {
    this.chan = await this.connection.createChannel();
    if (this.prefetch !== undefined) {
      await this.chan.prefetch(this.prefetch);
    }
  }

  protected handleOnClose() {
    this.log('log', 'amqp disconnected');
    this.connected = false;
    this.emit('close');

    if (this.doReconnect) {
      this.reconnect$.next();
    }
  }

  private log(level: 'log' | 'debug' | 'error', message: string) {
    if (this.showLogs) {
      this.logger[level](message);
    }
  }
}
